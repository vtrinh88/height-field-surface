#define TRACE_BACK
#define PI 3.14159265358979323
#define dfile 0

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cmath>
#include <math.h> 
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <iostream>

// =========== GLOBAL VARIABLES ======================================
// Variables for window
int button, state, mousex, mousey, dragX, dragY, cube = 1;
int width = 680, height = 680, isSpinning, lmax, do_neighbors = 2,
   tX = 0, tY = 0, tZ = 0, ttX = 0, ttY = 0, ttZ = 0, just_up = 0,
   tW = 0, ttW = 0;
double winRadius = 500, dX, dY;
double tt, transfac = .003, rad = 1.2,
   sumx = 0., sumy = 0., sumz = 0.;
double xxmin, xxmax, ymin, ymax, zmin, zmax, large = 1000000.;
clock_t ticks1, ticks2, ticks3, ticks4;
int iterations = 0, delay = 4000;
int use_checkImage = 0, single_slice = 0;
float mm[16];

// Variables for height field surface
int N_x = 15, N_y = 15;
float X_min = 0., X_max = 1., Y_min = 0., Y_max = 1.;
float n[3] = {0.,0.,0.};

// Variables for menu
static int win;
static int menuId;
static int submenuId;
static int primitive = 1; //default: flat shading

// ======== Function prototype ======================================
void display();
void createMenu();
void menu(int value);

// ==================================================================
// 			 SET OF FUNCTIONS FOR HEIGHT FIELD SURFACE 
// ==================================================================
float surface_function(float x, float y){
	float result = 0.;
	result = 0.13 + 0.5*sqrt(x*x+y*y) + 0.18*sin(30*(x+y))*cos(10*(x-y));
	return result;
}

float partialDerivative_onX(float x, float y){
	float df_dx = 0.;
	df_dx = (0.5*x)/(sqrt(x*x+y*y)) + 1.8*cos(20*x + 40*y) + 3.6*cos(40*x + 20*y);
	return df_dx;
}

float partialDerivative_onY(float x, float y){
	float df_dy = 0.;
	df_dy = (0.5*y)/(sqrt(x*x+y*y)) + 1.8*cos(40*x + 20*y) + 3.6*cos(20*x + 40*y);
	return df_dy;
}

void surface_normal(float x, float y){
	
	float dfdx = partialDerivative_onX(x,y);
	float dfdy = partialDerivative_onY(x,y);
	float dfdf = 1.;
	float norm = sqrt(dfdx*dfdx + dfdy*dfdy + dfdf*dfdf);
	
	//note***: -0.5 on each dimension for model translation
	n[0] = (dfdx*(-1)/norm) - 0.5;
	n[1] = (dfdy*(-1)/norm) - 0.5;
	n[2] = (dfdf/norm) - 0.5;
	
}

void drawFLAT_HeightFieldSurface(){
	float dx = (X_max - X_min)/N_x;
	float dy = (Y_max - Y_min)/N_y;
	
	glBegin(GL_QUADS);
	for (float x = X_min; x<=X_max - dx; x+=dx)
		for (float y = Y_min; y<=Y_max - dy; y+=dy){
			
			//note***: -0.5 on each dimension for model translation
			glVertex3f(x-.5,y-.5,surface_function(x,y)-.5);
			glVertex3f(x+dx-.5,y-.5,surface_function(x+dx,y)-.5);
			glVertex3f(x+dx-.5,y+dy-.5,surface_function(x+dx,y+dy)-.5);
			glVertex3f(x-.5,y+dy-.5,surface_function(x,y+dy)-.5);
		}
	glEnd();
}

void drawSMOOTH_HeightFieldSurface(){
	
	float dx = (X_max - X_min)/N_x;
	float dy = (Y_max - Y_min)/N_y;
	
	glBegin(GL_QUADS);
	for (float x = X_min; x<=X_max - dx; x+=dx)
		for (float y = Y_min; y<=Y_max - dy; y+=dy){
			//note***: -0.5 on each dimension for model translation
			
			surface_normal(x,y);
			printf("nx = %f, ny = %f, nz = %f\n",n[0],n[1],n[2]);
			glNormal3f(n[0],n[1],n[2]);
			glVertex3f(x-.5,y-.5,surface_function(x,y)-.5);
			
			surface_normal(x+dx,y);
			printf("nx = %f, ny = %f, nz = %f\n",n[0],n[1],n[2]);
			glNormal3f(n[0],n[1],n[2]);
			glVertex3f(x+dx-.5,y-.5,surface_function(x+dx,y)-.5);
			
			surface_normal(x+dx,y+dy);
			printf("nx = %f, ny = %f, nz = %f\n",n[0],n[1],n[2]);
			glNormal3f(n[0],n[1],n[2]);
			glVertex3f(x+dx-.5,y+dy-.5,surface_function(x+dx,y+dy)-.5);
			
			surface_normal(x,y+dy);
			printf("nx = %f, ny = %f, nz = %f\n\n",n[0],n[1],n[2]);
			glNormal3f(n[0],n[1],n[2]);
			glVertex3f(x-.5,y+dy-.5,surface_function(x,y+dy)-.5);
			
		}
	glEnd();	
}

// ==== reshape =====================================================
void reshape(int w, int h) {
   width = w;
   height = h;
   if (width > height)
      winRadius = (double)width*.5;
   else
      winRadius = (double)height*.5;

   glViewport(0, 0, (GLsizei) w, (GLsizei) h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(40.0, (GLfloat) w/(GLfloat) h, 0.1, 60.0);
   glTranslatef(0., 0., -4.);
   display();
   }

// ==== myRotated ==================================================
void myRotated(double a, double X, double Y, double Z) {

// Multiply on the right instead of the left

   float m[16], sx, sy, sz;
   glMatrixMode(GL_MODELVIEW);
   glGetFloatv(GL_MODELVIEW_MATRIX, m);
   glLoadIdentity();
   
   glTranslatef(transfac*ttX, transfac*ttY, transfac*ttZ);
   glRotated(a, X, Y, Z);
   glTranslatef(-transfac*ttX, -transfac*ttY, -transfac*ttZ);
   glMultMatrixf(m);
   }

// ==== doRotation ==================================================
void doRotation(double dX, double dY) {
    double offX = 2.0*(double)dragX/(double)width-1.0;
    double offY = 1.0-2.0*(double)dragY/(double)height;
    double offZ = 0.25;
    double X, Y, Z, s, c, t, d, a;
    X = -offZ*dY;
    Y = offZ*dX;
    Z = offX*dY - offY*dX;
    a = 180.*1.5*sqrt(dX*dX + dY*dY)/(PI*winRadius);
    myRotated(a, X, Y, Z);
    return;
    }

// ==== doTranslation ==================================================
void doTranslation(int tX, int tY, int tZ) {

// Multiply on the right instead of the left

   float m[16];
   glMatrixMode(GL_MODELVIEW);
   glGetFloatv(GL_MODELVIEW_MATRIX, m);
   glLoadIdentity();
   glTranslatef(transfac*tX, transfac*tY, transfac*tZ);
   glMultMatrixf(m);
   }

//====== Vector Cross product function ==================================
//  inputs: vector v, w, a
//  output: a = v x w

void vcross(double v[3], double w[3], double a[3]) {
   a[0] = v[1]*w[2] - v[2]*w[1];
   a[1] = v[2]*w[0] - v[0]*w[2];
   a[2] = v[0]*w[1] - v[1]*w[0];
   }

// ==== display ========================================================
void display(void) {
   int k, j, i, l, m, iw, mstop, sum = 0;
   double t, s, c, xx, yy, zz;
   ticks4 = clock();
   tt = (double)(ticks4 - ticks3)/(double)CLOCKS_PER_SEC;
   if(state == GLUT_DOWN && tt > .05) isSpinning = 0;
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   j = iterations - delay;
   //glColor4f(1., .2, 0.2, .7);
   if(isSpinning) {
      glMatrixMode(GL_MODELVIEW);
      doRotation(dX, dY);
   	  }
  
   // ROUTINE POLYGON CALLS 
   if (primitive == 1){
	  glShadeModel(GL_FLAT);
      glutWireCube(1.); 
      drawFLAT_HeightFieldSurface();
      }
   else if (primitive == 2){
	  glShadeModel(GL_SMOOTH);
	  glutWireCube(1.); 
	  drawSMOOTH_HeightFieldSurface();
      }
   // =======================

   glutSwapBuffers();
   glFlush();
   ++iterations;
   }

// ==== mouse ===========================================================
void mouse(int but, int sta, int x, int y) {
   button = but;
   state = sta;
   if(state == GLUT_DOWN) {
      dragX = x;
      dragY = y;
      dX = 0;
      dY = 0;
      }
   if (state == GLUT_UP && button == GLUT_LEFT_BUTTON) {
/*
      dX = x - dragX;
      dY = dragY - y;
      if(dX!=0||dY!=0)
         isSpinning = 1;
      else
         isSpinning = 0;
*/
      ticks1 = clock();
      isSpinning = 0;
      just_up = 1;
      }
   }

// ==== passive motion ==================================================
void passive_motion(int x, int y) {
   double t;
   if(just_up) {
      ticks2 = clock();
      t = (double)(ticks2 - ticks1)/(double)CLOCKS_PER_SEC;
      just_up = 0;
      if (t < .01) {
         dX = .2*(x - dragX);
         dY = .2*(dragY - y);
	 isSpinning = 1;
	 }
      }
   }

// ==== motion ===========================================================
void motion(int x, int y) {
   if (state == GLUT_DOWN && button == GLUT_LEFT_BUTTON) {
      dX = x - dragX;
      dY = dragY - y;
      glMatrixMode(GL_MODELVIEW);
      doRotation(dX, dY);
      dragX = x;
      dragY = y;
      }
   if (state == GLUT_DOWN && button == GLUT_MIDDLE_BUTTON) {
      tX = x - dragX;
      tY = dragY - y;
      ttX += tX;
      ttY += tY;
      dragX = x;
      dragY = y;
      doTranslation(tX, tY, 0);
      }
   if (state == GLUT_DOWN && button == GLUT_RIGHT_BUTTON) {
      tZ = x - dragX;
      ttZ += tZ;
      tW = y - dragY;
      ttW += tW;
      dragX = x;
      dragY = y;
      doTranslation(0, 0, tZ);
      }
   ticks3 = clock();
   tt = (double)(ticks3 - ticks4)/(double)CLOCKS_PER_SEC;
   display();
   }

// ==== initScreen ==================================================
void initScreen(){
   GLfloat light_position[4] = {1., 14., 10., 0.};
   GLfloat white_light[4]  = {1., 1., .6, 0.};
   GLfloat lmodel_ambient[4] = {.3, .8, .37, 1.};
   GLfloat lmodel_specular[3] = {.5, .5, .5};
	
	// ----- initial setup for height field surface -------
    //glShadeModel(GL_FLAT);
	glColor3f(0.3,0.8,0.37);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0,GL_AMBIENT,lmodel_ambient);
	glLightfv(GL_LIGHT0,GL_SPECULAR,lmodel_specular);
	glLightfv(GL_LIGHT0,GL_POSITION,light_position);
	
	// ----- end initial setup ----------------------------
	
   glClearColor(0., 0., 0., 0.);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   // glOrtho(-.9, .9, -.9, .9, -1., 1.);
   if(0)
      glTranslatef(0., 0., -4.);
   else
      glTranslatef(0., 0., -4.);
   
   gluPerspective(40., width/height, 0.1, 60.);
   glEnable(GL_POINT_SMOOTH);
   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE);
   }

// ==== Main function ================================================
int main(int argc, char** argv) {
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
   glutInitWindowSize(width, height);
   glutInitWindowPosition(0, 0);
   win = glutCreateWindow("Height Field Surface");
   initScreen();
   createMenu();
   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutMouseFunc(mouse);
   glutMotionFunc(motion);
   glutPassiveMotionFunc(passive_motion);
   glutMainLoop();
   return 0;
   }

void createMenu(){
	// Create submenu first
	submenuId = glutCreateMenu(menu);
	glutAddMenuEntry("Flat Shading",1);
	glutAddMenuEntry("Smooth Shading",2);
	
	// Create main menu
	menuId = glutCreateMenu(menu);
	glutAddSubMenu("View",submenuId);
	glutAddMenuEntry("Exit",0);
	
	// Associate Menu with right mouse button
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void menu(int value){
	if(value == 0){
		glutDestroyWindow(win);
		exit(0);
	}
	else
		primitive = value;
		
	glutPostRedisplay();
}

